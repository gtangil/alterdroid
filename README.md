Alterdroid
=========
 
Alterdroid is a dynamic analysis tool for detecting hidden or obfuscated malware components distributed as parts of an app package. The key idea in Alterdroid consists of analyzing the behavioral differences between the original app and a number of automatically generated versions of it where a number of modifications (faults) have been carefully injected. Observable differences in terms of activities that appear or vanish in the modified app are recorded, and this signature is finally analyzed through a pattern-matching process driven by rules that relate different types of hidden functionalities with patterns found in the differential signature.
 
Version
----
 
1.0
 
Tech
-----------
 
Alterdroid uses the following Operating Systems:
 
* [Ubuntu Server or Desktop]
 
Install Java
--------------
 
```sh
sudo apt-get install default-jre

sudo apt-get install default-jdk
```
Install Vi
--------------
 
```sh
sudo apt-get install vim
```
 
Install Android
--------------
 
```sh
wget http://dl.google.com/android/android-sdk_r23.0.2-linux.tgz

tar -xvzf android-sdk_r23.0.2-linux.tgz

vi ~/.bashrc
```
 
Include in file named bashrc the next lines:
 
* export PATH=${PATH}:~/android-sdk-linux/tools
* export PATH=${PATH}:~/android-sdk-linux/platform-tools
 
```sh
cd ~/android-sdk-linux/tools

android update sdk --no-ui

sudo apt-get install libc6:i386 libstdc++6:i386 libgl1-mesa-dev:i386

android update sdk --filter android-10 --no-ui
 
```
 
Install Androguard
--------------
 
Follow the instructions here: https://code.google.com/p/androguard/
 
The steps are as follows:
 
```sh
sudo apt-get install mercurial python python-setuptools g++
 
sudo apt-get install python-dev libbz2-dev libmuparser-dev libsparsehash-dev python-ptrace python-pygments python-pydot graphviz liblzma-dev libsnappy-dev
 
sudo apt-get install python-dev libbz2-dev libmuparser-dev libsparsehash-dev python-ptrace python-pygments python-pydot graphviz liblzma-dev libsnappy-dev
 
hg clone https://androguard.googlecode.com/hg/ androguard
```
 
Install Phyton
--------------
 
Follow the instructions here: https://code.google.com/p/pythonxy/
 
The steps are as follows:
 
```sh
cd ~/Downloads/

wget http://python.org/ftp/python/2.7.5/Python-2.7.5.tgz

tar -xvf Python-2.7.5.tgz

cd Python-2.7.5

./configure

make

sudo make install
```
 
Install Apktool
--------------
 
Follow the instructions here: https://code.google.com/p/android-apktool/wiki/Install
 
The steps are as follows:
 
```sh
wget https://raw.githubusercontent.com/iBotPeaches/Apktool/master/scripts/windows/apktool.bat

wget https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_2.0.0rc2.jar

mv apktool_2.0.0rc2.jar apktool.jar

sudo mv apktool* /usr/local/bin/

sudo chmod +x /usr/local/bin/apktool*
```
 
Install Androidviewclient
--------------
 
Follow the instructions here:  https://github.com/dtmilano/AndroidViewClient/wiki
 
For download Androidviewclient, you have to execute the next comando:
 
```sh
sudo easy_install --upgrade androidviewclient
```
 
Install Droidbox
--------------
 
Follow the instructions here: https://code.google.com/p/droidbox/
 
First, download the files and uncompress it:
 
```sh
wget http://droidbox.googlecode.com/files/DroidBox411RC.tar.gz

tar -xvzf DroidBox411RC.tar.gz
```
 
Setup a new AVD targeting Android 4.1.2 and choose Nexus 4 as device as well as ARM as CPU type by running:
 
```sh
android
```
 
Other dependencies
--------------
 
```sh
sudo apt-get install python-imaging

sudo apt-get install python-levenshtein

sudo apt-get install python-numpy python-scipy python-matplotlib

sudo apt-get install python-magic

sudo apt-get install imagemagick
```
 
Examples for execution
--------------
 
```sh
./alterdroid.sh -d samples

./alterdroid.sh -d samples -t 120

./alterdroid.sh -d samples -t 120 -f
```