#!/usr/bin/env python
###############################################################################################
# (c) 2014, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid. All rights reserverd.
# Main Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
###############################################################################################

import sys, os, shutil, tempfile, ntpath
import uuid
from subprocess import Popen, call, PIPE

OUT_BASE_PATH = "samples_repackaged"
SAMPLE_NAME = "sample"

# TODO: This is a bit ugly, but it works for the moment.
#PATH_TO_APKTOOL = "libs/apktool1.5.2/"
PATH_TO_APKTOOL = "libs/apktool1.5.2-linux/"
APKTOOL = "apktool.jar"

APKTOOL_LIB = os.path.join(os.getcwd(), PATH_TO_APKTOOL)
APKTOOL_JAR = os.path.join(APKTOOL_LIB, APKTOOL)
PATH =  os.environ.get("PATH", '')
PATH = PATH + ":" + APKTOOL_LIB


# ------------------ 

class FIO(object):
    def __init__(self, apk):
        self.type = None
        self.apk = apk
    
    def get_apk(self):
        return self.apk

    def unpackage(self):
        # Get orifinal file
        file_name = self.apk.get_filename()
        path_to_apk = os.path.join(os.getcwd(), file_name)
        
        # Create a working space
        out_base_dir = os.path.join(os.getcwd(), OUT_BASE_PATH)
        out_base_file = file_name.replace("/", "_").replace(".apk", "")
        path_to_output_dir = os.path.join(out_base_dir, out_base_file)
        path_to_unpackaged_dir = os.path.join(path_to_output_dir, SAMPLE_NAME)
        
        if not os.path.exists(path_to_output_dir):
            os.makedirs(path_to_output_dir)
        
        path_to_output_original_file = os.path.join(path_to_output_dir, SAMPLE_NAME + ".apk")
        shutil.copyfile(path_to_apk, path_to_output_original_file)
                            
        # Unpackage the orifinal file into the working directory 
    
        cmd = ['java', '-jar', APKTOOL_JAR, 'decode', path_to_output_original_file, path_to_unpackaged_dir]
        try:
            call(cmd, env={"PATH": PATH}) #, stderr=PIPE)
            print "UNPACKAGED: ", path_to_output_original_file, " into ", path_to_unpackaged_dir
        except OSError as e:
            print "Error trying to run APKTOOL from ", APKTOOL_JAR, ". Make sure java is installed and apktool lib properly referenced."
            print "OSError ({0}): {1}".format(e.errno, e.strerror)
            print sys.exc_info()
            raise

        return path_to_output_dir, path_to_unpackaged_dir

    def repackage(self, path_to_output_dir, path_to_unpackaged_injected_dir):
        
        base_name = os.path.basename(path_to_unpackaged_injected_dir)
        if base_name is '':
            name = os.path.basename(os.path.dirname(path_to_unpackaged_injected_dir)) + ".apk"
            real_path = os.path.realpath(path_to_unpackaged_injected_dir + "..")
        else:
            name = base_name + ".apk"
            real_path = os.path.realpath(path_to_unpackaged_injected_dir + "/..")
        
        if path_to_output_dir is None:
            path_to_resulting_repackaged_apk = os.path.join(real_path, name)
        else:
            path_to_resulting_repackaged_apk = os.path.join(path_to_output_dir, name)
            #tempfile.mktemp(prefix="repackaged" + '_', suffix='.apk', dir=path_to_output_dir)
        
        cmd = ['java', '-jar', APKTOOL_JAR, 'build', path_to_unpackaged_injected_dir, path_to_resulting_repackaged_apk]
        try:
            call(cmd, env={"PATH": PATH})#, stderr=PIPE)
            print "REPACKAGED: ", path_to_unpackaged_injected_dir, " into ", path_to_resulting_repackaged_apk
        except OSError as e:
            print "Error trying to run APKTOOL from ", APKTOOL_JAR, ". Make sure java is installed and apktool lib properly referenced."
            print "OSError ({0}): {1}".format(e.errno, e.strerror)
            print sys.exc_info()
            raise
                
        #TODO: os.remove(FILE)
                
        return path_to_resulting_repackaged_apk

    def cloneAPK(self, path_to_unpackaged_dir, sub_type, path_to_output_dir = None):
        if path_to_output_dir is None:
            rand_name = str(uuid.uuid4())
            path_to_resulting_repackaged_injected_apk = path_to_unpackaged_dir + "_" + self.sub_type + "_" + rand_name
        else:
            path_to_resulting_repackaged_injected_apk = path_to_output_dir
        shutil.copytree(path_to_unpackaged_dir, path_to_resulting_repackaged_injected_apk)
        return path_to_resulting_repackaged_injected_apk

class FioNotSupportedByAlterdroid(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)