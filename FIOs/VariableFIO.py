#!/usr/bin/env python
###############################################################################################
# (c) 2014, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid. All rights reserverd.
# Main Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
###############################################################################################

import sys

from FIO import FIO, FioNotSupportedByAlterdroid

# ------------------

class VariableFIO(FIO):
    def __init__(self, apk, attributes):
        self.type = self.__class__.__name__
        FIO.__init__(self, apk)
    #TODO
    
    
    def get_fio_handler(self, coi, attributes):
        fios = self.select_fios(coi)
        for fio in fios:
            try:
                klass = getattr(sys.modules[__name__], fio)
                yield klass(coi, attributes)
            except AttributeError:
                raise FioNotSupportedByAlterdroid('Event not supported by clone: ' + fio)
    
    def select_fios(self, coi):
        #TODO
        return []

class TemplateVariableCheck(VariableFIO):
        
    def __init__(self, coi, subattributes):
        self.coi = coi
        self.sub_type = self.__class__.__name__
        #TODO: Quet the attributes for this FIO
            
    def sub_type(self):
        return self.sub_type
    
    def inject_fault(self):
        #TODO: Add functinoallity here
        pass

