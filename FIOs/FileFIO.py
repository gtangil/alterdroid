#!/usr/bin/env python
###############################################################################################
# (c) 2014, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid. All rights reserverd.
# Main Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
###############################################################################################

import sys, os, shutil
from subprocess import call
from FIO import FIO, FioNotSupportedByAlterdroid

from subprocess import Popen, PIPE, STDOUT
#import Image

class FileFIO(FIO):
    
    map_fio_coi = {"ImageFileExtensionMismatch": "GenericMutationFile",
                    "ImageFileMatch": "ImageFile",
                    "RAtCFileMatch": "ExecutableFile",
                    "TextScriptMatch": "ScriptFile",
                    "EncryptedOrCompressedMatch":"GenericMutationFile",
                    "APKFileMatch": "APKFile",
                    "DEXFileMatch": "DEXFile",
                    "ELFExecutableMatch": "ELFFile",
                    "APKFileExtensionMismatch": "APKFile"}
    
    def __init__(self, apk, attributes):
        self.type = self.__class__.__name__
        #FIO.__init__(self, apk)
        self.attributes = attributes
        super(FileFIO,self).__init__(apk)
        self.extension = attributes[2]
        self.magic_key = attributes[3]
        self.magic_description = attributes[4]
        self.path = attributes[5]
        self.crc = attributes[6]

    def get_fio_handler(self, coi, subattributes):
        fios = self.select_fios(coi)
        for fio in fios:
            try:
                klass = getattr(sys.modules[__name__], fio)
                yield klass(self.apk, self.attributes, coi, subattributes)
            except AttributeError:
                raise FioNotSupportedByAlterdroid('Event not supported by clone: ' + fio)
    
    def select_fios(self, coi):
        for item in self.map_fio_coi:
            if item is coi.get_sub_type():
                yield self.map_fio_coi[item]

    def generic_mutation(self, path_to_inject):
        # TODO: sophisticate the injection:
        #size = os.path.getsize(path_to_inject)
        #f.seek(...) <- random
        
        f = open(path_to_inject, 'wr')
        f.write("Random string: asjlfdasldjfalskdjfal")
        f.close()
                    
    def image_mutation(self, path_to_inject):
        # TODO: sophisticate the injection as this is vulnerable to some stego alg.(e.g.: F5)
        #call(['convert', path_to_inject, '-flip', path_to_inject])
        call(["convert", path_to_inject, "-draw", "text 0,0 'Alterdroid'", path_to_inject])

# ------------------

class GenericMutationFile(FileFIO):
    
    def __init__(self, apk, attributes, coi, subattributes):
        super(GenericMutationFile,self).__init__(apk, attributes)
        self.coi = coi
        self.sub_type = self.__class__.__name__
    
    def get_sub_type(self):
        return self.sub_type
    
    def inject_fault(self, path_to_unpackaged_dir, path_to_resulting_repackaged_injected_apk = None):
        
        if path_to_resulting_repackaged_injected_apk is None:
            path_to_resulting_repackaged_injected_apk = self.cloneAPK(path_to_unpackaged_dir, self.sub_type)
        path_to_inject = os.path.join(path_to_resulting_repackaged_injected_apk, self.path)
        self.generic_mutation(path_to_inject)
        return path_to_resulting_repackaged_injected_apk

# ------------------

class ImageFile(FileFIO):
    
    def __init__(self, apk, attributes, coi, subattributes):
        super(ImageFile,self).__init__(apk, attributes)
        self.coi = coi
        self.sub_type = self.__class__.__name__
        
    def get_sub_type(self):
        return self.sub_type
    
    def inject_fault(self, path_to_unpackaged_dir, path_to_resulting_repackaged_injected_apk = None):
        
        if path_to_resulting_repackaged_injected_apk is None:
            path_to_resulting_repackaged_injected_apk = self.cloneAPK(path_to_unpackaged_dir, self.sub_type)
        path_to_inject = os.path.join(path_to_resulting_repackaged_injected_apk, self.path)
        
        if "image" in self.magic_description:
            self.image_mutation(path_to_inject)
        else:
            self.generic_mutation(path_to_inject)
        
        return path_to_resulting_repackaged_injected_apk


# ------------------

'''
    Text Script FIO.
'''
class ScriptFile(FileFIO):
    
    def __init__(self, apk, attributes, coi, subattributes):
        super(ScriptFile,self).__init__(apk, attributes)
        self.coi = coi
        self.sub_type = self.__class__.__name__
    
    def get_sub_type(self):
        return self.sub_type
    
    def inject_fault(self, path_to_unpackaged_dir, path_to_resulting_repackaged_injected_apk = None):
        
        if path_to_resulting_repackaged_injected_apk is None:
            path_to_resulting_repackaged_injected_apk = self.cloneAPK(path_to_unpackaged_dir, self.sub_type)
        path_to_inject = os.path.join(path_to_resulting_repackaged_injected_apk, self.path)
        
        nullScript = open(path_to_inject, 'r+')
        firstLine = nullScript.readline()
        if firstLine[0:3] == '#!/':
            nullScript.write(firstLine)
        else:
            nullScript.write('#!/system/bin/sh\n')
        nullScript.close()
        
        return path_to_resulting_repackaged_injected_apk



# ------------------

'''
APK FIO. Injects an innocuous APK previously generated, i.e.: 'alterdroid.apk'.
'''
class APKFile(FileFIO):
    
    def __init__(self, apk, attributes, coi, subattributes):
        super(APKFile,self).__init__(apk, attributes)
        self.coi = coi
        self.sub_type = self.__class__.__name__
    
    def get_sub_type(self):
        return self.sub_type
    
    def inject_fault(self, path_to_unpackaged_dir, path_to_resulting_repackaged_injected_apk = None):
        
        if path_to_resulting_repackaged_injected_apk is None:
            path_to_resulting_repackaged_injected_apk = self.cloneAPK(path_to_unpackaged_dir, self.sub_type)
        path_to_inject = os.path.join(path_to_resulting_repackaged_injected_apk, self.path)
        
        # -- Injecting an innocuous APK
        shutil.copyfile('alterdroid.apk', path_to_inject)
        
        return path_to_resulting_repackaged_injected_apk


# ------------------

'''
    DEX FIO. Injects an innocuous Dex previously generated, i.e.: 'alterdroid.dex'.
    '''
class DEXFile(FileFIO):
    
    def __init__(self, apk, attributes, coi, subattributes):
        super(DEXFile,self).__init__(apk, attributes)
        self.coi = coi
        self.sub_type = self.__class__.__name__
    
    def get_sub_type(self):
        return self.sub_type
    
    def inject_fault(self, path_to_unpackaged_dir, path_to_resulting_repackaged_injected_apk = None):
        
        if path_to_resulting_repackaged_injected_apk is None:
            path_to_resulting_repackaged_injected_apk = self.cloneAPK(path_to_unpackaged_dir, self.sub_type)
        path_to_inject = os.path.join(path_to_resulting_repackaged_injected_apk, self.path)
        
        # -- Injecting an innocuous DEX
        shutil.copyfile('alterdroid.dex', path_to_inject)
        
        return path_to_resulting_repackaged_injected_apk

# ------------------

'''
    ELF FIO. Injects an innocuous Dex previously generated, i.e.: 'alterdroid.dex'.
'''
class ELFFile(FileFIO):
    
    def __init__(self, apk, attributes, coi, subattributes):
        super(ELFFile,self).__init__(apk, attributes)
        self.coi = coi
        self.sub_type = self.__class__.__name__
    
    def get_sub_type(self):
        return self.sub_type
    
    def inject_fault(self, path_to_unpackaged_dir, path_to_resulting_repackaged_injected_apk = None):
        
        if path_to_resulting_repackaged_injected_apk is None:
            path_to_resulting_repackaged_injected_apk = self.cloneAPK(path_to_unpackaged_dir, self.sub_type)
        path_to_inject = os.path.join(path_to_resulting_repackaged_injected_apk, self.path)
        
        # -- Injecting an innocuous ELF
        shutil.copyfile('alterdroid.elf', path_to_inject)
        
        return path_to_resulting_repackaged_injected_apk

# ------------------

'''
   Template. 
    (1) Make it extend from FileFIO 
    (2) Add the functionality
    (3) Modify 'map_fio_coi' to map this FIO with a CoI
'''
class TemplateFileFIO:#(FileFIO):
    
    def __init__(self, apk, attributes, coi, subattributes):
        super(TemplateFileFIO,self).__init__(apk, attributes)
        self.coi = coi
        self.sub_type = self.__class__.__name__
    
    def get_sub_type(self):
        return self.sub_type
    
    def inject_fault(self, path_to_unpackaged_dir, path_to_resulting_repackaged_injected_apk = None):
        
        if path_to_resulting_repackaged_injected_apk is None:
            path_to_resulting_repackaged_injected_apk = self.cloneAPK(path_to_unpackaged_dir, self.sub_type)
        path_to_inject = os.path.join(path_to_resulting_repackaged_injected_apk, self.path)
        
        # TODO: Add functinoallity here
        
        return path_to_resulting_repackaged_injected_apk

