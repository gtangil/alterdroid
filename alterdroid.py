
#!/usr/bin/env python
###############################################################################################
# (c) 2014, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid. All rights reserverd.
# Main Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
###############################################################################################

import sys, os, inspect, signal, StringIO
#re, cmd, code, re
from time import sleep
from subprocess import Popen, PIPE, STDOUT, call
import threading
from threading import Thread
import multiprocessing
from multiprocessing import Process, Queue

from tempfile import mkstemp
from shutil import move, rmtree
from os import remove, close

#try:
#    from subprocess import SubprocessError, TimeoutExpired
#except ImportError:
#    print "You need Python 3.3"
#    raise

from optparse import OptionParser

import androguard
from androguard.core.androgen import *
from androguard.core.bytecodes import apk, dvm
from androguard.core.analysis import analysis, ganalysis
from androguard.core import androconf

import CoIs
from CoIs import *
#from CoIs.FileCoI import *
#from CoIs.VariableCoI import *

import FIOs
from FIOs import *

from Levenshtein import *

import droidbox
from droidbox.cosec_droidbox import *


# PyDev sets PYTHONPATH, use it
try:
    for p in os.environ['PYTHONPATH'].split(':'):
        if not p in sys.path:
            sys.path.append(p)
except:
    pass

try:
    sys.path.append(os.path.join(os.environ['ANDROID_VIEW_CLIENT_HOME'], 'src'))
except:
    pass
from com.dtmilano.android import viewclient as vc
from com.dtmilano.android.viewclient import ViewClient


# TODO: This is a bit ugly, but it works for the moment.
#PATH_TO_ANDROIDVIEW = "libs/AndroidViewClient"
#PATH_TO_ANDROIDVIEW_LIB = os.path.join(os.getcwd(), PATH_TO_ANDROIDVIEW)
#ANDROID_VIEW_CLIENT_HOME =  os.environ.get("ANDROID_VIEW_CLIENT_HOME", '')
#ANDROID_VIEW_CLIENT_HOME = PATH_TO_ANDROIDVIEW_LIB

'''
    The console port number must be an even integer between 5554 and 5584,
    inclusive.
    NOTE: The emulator console port number must be an even integer between 5554 and 5584, inclusive. <port>+1 must also be free and will be reserved for ADB. Thus, we can only parallize 16 emulators.
    '''

STARTING_POOL_PORT = 5554
PROCESSES = 16
PORTS = Queue()
for i in range(PROCESSES):
    PORTS.put((i*2) + STARTING_POOL_PORT)

version = "0.1"

option_0 = { 'name' : ('-i', '--input'), 'help' : 'file : inject faults in this apk', 'nargs' : 1 }
option_1 = { 'name' : ('-v', '--version'), 'help' : 'version of the API', 'action' : 'count' }
option_2 = { 'name' : ('-d', '--directory'), 'help' : 'directory : use this directory, test all apks', 'nargs' : 1 }
option_3 = { 'name' : ('-t', '--time'), 'help' : 'time : time of the dynamic analysis', 'nargs' : 1 }
option_4 = { 'name' : ('-f', '--fast'), 'help' : 'fast : applies all the injections at once', 'action' : 'count' }
option_5 = { 'name' : ('-c', '--clean'), 'help' : 'clean : delete resulting repackaged apks', 'action' : 'count' }
option_6 = { 'name' : ('-p', '--port'), 'help' : 'port : starting pool Android emulator port', 'nargs' : 1 }
option_7 = { 'name' : ('-s', '--caseStudy'), 'help' : 'file_original file_fio : computes the differential signature', 'nargs' : 2 }



options = [option_0, option_1, option_2, option_3, option_4, option_5, option_6, option_7]

alterdroid_prefix = 'alterdroid'


# -----------------------
# Properties
# -----------------------

property_del_net_open = ["delete", "net-open"]
property_del_net_read = ["delete", "net-read"]
property_del_net_write = ["delete", "net-write"]

property_del_file_open = ["delete", "file-open"]
property_del_file_read = ["delete", "file-read"]
property_del_file_write = ["delete", "file-write"]

property_del_dexload = ["delete", "dexload"]
property_del_leak = ["delete", "leak"]
property_del_sms = ["delete", "sms"]
property_del_crypto = ["delete", "crypto"]

#property_del_ = ["delete", "-"]


# -----------------------
# Rules
# -----------------------

# -- Rule: Contains Net. Activity Componet  
Rnac = {"name":"NAC", "or":[property_del_net_open, property_del_net_read, property_del_net_write]}

# -- Rule: Contains File Activity Componet 
Rfac = {"name":"FAC", "and":[], "or":[property_del_file_open, property_del_file_read, property_del_file_write]}

# -- Rule: Contains Data Leakage Component
Rdlc = {"name":"DLC", "or":[property_del_leak]}

# -- Rule: Contains SMS Activity Componet
Rsac = {"name":"SAC", "or":[property_del_sms]}

# -- Rule: Contains Payload Activity Componet
Rpac = {"name":"PAC", "and":[property_del_dexload]}

# -- Rule: Contains Update Activity Componet
Rupd = {"name":"UPD", "and":[property_del_net_read, property_del_dexload]}

# -- Rule: Contains Crypto. Activity Componet
Rcac = {"name":"CAC", "and":[property_del_crypto]}

# -- Rule: Contains Payload Componet
Rcpc = {"name":"CPC", "and":[property_del_crypto, property_del_dexload]}

# -- Rule: Contains Hidden Functionality Component
Rhfc = {"name":"HFC", "and":[], "or":[]} # TODO

# -- Rule: TODO
Rtodo = {"name":"", "and":[], "or":[]}

rules = [Rnac, Rfac, Rdlc, Rsac, Rpac, Rupd, Rcac, Rcpc, Rhfc]

#TODO: Implement rules in boolean algebra (this is a mock approximation)


# -----------------------
# Classes
# -----------------------

class InteractionThread(Thread):
    """
    Run until the main Activity
    within an APK have been started
    """
    running = False
    interacted = []

    def __init__ (self, apk, port='5554', test=True, time_duration=0):
        """
        Constructor
        """
        self._apk = apk
        self._port = port
        self._test = test
        self.duration = time_duration
        Thread.__init__(self)
    
    def stop(self):
        self.running = False
    
    
    def install(self, apk, port):
        
        self.interacted = []
        
        # obtain activity components of the apk
        package = apk.get_package()
        activities = apk.get_activities()
        main_activity = apk.get_main_activity()
        emulator = get_emulator_name(port)
        
        # install app: https://github.com/dtmilano/AndroidViewClient/issues/53
        status = call(['adb', '-s', emulator, 'install', apk.get_filename()])
    
    
    def install_and_run(self, apk, port, runnerNotAndroidViewClient=False):
        
        self.interacted = []

        # obtain activity components of the apk
        package = apk.get_package()
        activities = apk.get_activities()
        main_activity = apk.get_main_activity()
        emulator = get_emulator_name(port)
        
        # install app: https://github.com/dtmilano/AndroidViewClient/issues/53
        status = call(['adb', '-s', emulator, 'install', apk.get_filename()])
        
        print ">>> Running activities... "
        
        time_main = int(0.1 * self.duration)
        
        if self.duration - time_main <= 0 or self.duration is 0:
            time_per_view = 1
        else:
            time_per_view = int((self.duration - time_main)/self.duration)
        
        # Run main activity
        if runnerNotAndroidViewClient:
            # connect to the device through MonekeyRunner
            device, serialno = ViewClient.connectToDeviceOrExit(verbose=False, serialno=emulator)
            self.run_activty(device, package, main_activity, time_main)
        else:
            # connect to the device through AndroidViewClient
            self.run_activity_callRunner(package, main_activity, time_main)

        # Run the reminder
        for activity in activities:
            if not self.running:
                break
            if activity != main_activity:
                print "> Running: ", activity
                if runnerNotAndroidViewClient:
                    # connect to the device through MonekeyRunner
                    device, serialno = ViewClient.connectToDeviceOrExit(verbose=False, serialno=emulator)
                    self.run_activty(device, package, activity, time_per_view)
                else:
                    # connect to the device through AndroidViewClient
                    self.run_activity_callRunner(package, activity, time_per_view)


    def install_and_test(self, apk, port):
        
        self.interacted = []
        
        # obtain activity components of the apk
        package = apk.get_package()
        activities = apk.get_activities()
        main_activity = apk.get_main_activity()
        
        # connect to the device through AndroidViewClient
        emulator = get_emulator_name(port)
        device, serialno = ViewClient.connectToDeviceOrExit(verbose=False, serialno=emulator)
        
        # install app: https://github.com/dtmilano/AndroidViewClient/issues/53
        status = call(['adb', '-s', emulator, 'install', apk.get_filename()])
        
        # interact with the main activity
        self.run_activty_and_interact(device, serialno, package, main_activity)
        
        for activity in activities:
            if not self.running:
                break
            if activity != main_activity:
                self.run_activty_and_interact(device, serialno, package, activity)

    def run_activity_callRunner(self, package, activity, time_per_view=1):
        if package != None and activity != None:
            status = call(['monkeyrunner', 'alterdroidRunner.py', package, activity])
            sleep(time_per_view)

    def run_activty(self, device, package, activity, time_per_view=1):
        
        runComponent = package + '/' + activity
        # Run activity component
        if package != None and activity != None:
            device.startActivity(component=runComponent)
            ViewClient.sleep(time_per_view)
            print ">>> running", runComponent
        else:
            print ">>> Not able to run " + runComponent


    def run_activty_and_interact(self, device, serialno, package, activity, interactVisible=True):
        
        time_main = int(0.1 * self.duration)
        
        if self.duration - time_main <= 0 or self.duration is 0:
            time_per_view = 1
        else:
            time_per_view = int((self.duration - time_main)/self.duration)
        
        # Run activity component
        run_activty(self, device, package, activity, time_per_view=1)
        
        # Interact
        kwargs2 = {'autodump': False}
        vc = ViewClient(device, serialno, **kwargs2)
        if interactVisible == True:
            self.interactDumpCurrent(device, serialno, vc, time_per_view)
        else:
            self.interactDumpWindow(device, serialno, vc, time_per_view)


    def interactDumpWindow(self, device, serialno, vc, time_per_view):
        windows = vc.list()
        for wId in windows.keys():
            #if package in windows[wId]:
            views = vc.dump(window=wId, sleep=time_per_view)
            for view in views:
                if not self.running:
                    break
                self.interactView(vc, view)


    def interactDumpCurrent(self, device, serialno, vc, time_per_view):
        views = vc.dump(sleep=time_per_view)
        for view in views:
            if not self.running:
                break
            self.interactView(vc, view)


    def interactView(self, vc, view):
        
        if not self.running:
            pass
        
        b = vc.findViewWithText('Force close')
        if b:
            #print "<<<<<<<<< Force close", b.getId(), b.getText(), b.getXY(), b.isClickable()
            b.touch()
        
        if 'android.widget.EditText' == view.getClass():
            view.type(alterdroid_prefix)
        
        if view and view.isClickable() and not view.getId() in self.interacted:
            print ">>> view=", view.getId(), view.getText(), view.getXY(), view.isClickable()
            view.touch()
            self.interacted.append(view.getUniqueId())
            #self.interact(device, serialno, vc, time_per_view)


    '''
        Return the number of windows and views a package has
        '''
    def count_views(self, package, port='5554'):
        emulator = get_emulator_name(port)
        kwargs2 = {'autodump': False}
        vc = ViewClient(*ViewClient.connectToDeviceOrExit(verbose=False, serialno=emulator), **kwargs2)
        windows = vc.list()
        num_views = 0
        for wId in windows.keys():
            # count views from a given package
            if package in windows[wId]:
                views = vc.dump(window=wId)
                num_views += len(views)
        
        return len(windows.keys()), num_views

    def run(self):
        """
        Run all the activities and interact with them
        """
        try:
            self.running = True
            if self._test:
                self.install_and_test(self._apk, self._port)
            else:
                self.install_and_run(self._apk, self._port)
        except Exception, e:
            # -- delete path_to_output_dir
            print_error =  "[ERROR INTERACTION] Not possible to interact with the app " + self._apk + " due to:  " + str(e)
            print print_error
            import traceback
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_tb(exc_traceback)
            error = open('error.txt', 'a+')
            error.write(print_error + "\n")
            error.close()

# -----------------------
# Logo
# -----------------------

alterdroid_logo = '''
##########################################################################
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es   #
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid #
##########################################################################
#                                                                        #
#                                                                        #
#                                                                        #
#   .d8888b .d88b.  .d8888b   .d88b.   .d8888b                           #
#  d88P"   d88""88b 88K      d8P  Y8b d88P"                              #
#  888     888  888 "Y8888b. 88888888 888                                #
#  Y88b.   Y88..88P      X88 Y8b.     Y88b.                              #
#   "Y8888P "Y88P"   88888P\'  "Y8888   "Y8888P                           #
#                                                                        #
#                                                                        #
#                                                                        #
#                    .d8888b.                                            #
#                   d88P  Y88b                                           #
#                        .d88P                                           #
#  888  888  .d8888b    8888"  88888b.d88b.       .d88b.  .d8888b        #
#  888  888 d88P"        "Y8b. 888 "888 "88b     d8P  Y8b 88K            #
#  888  888 888     888    888 888  888  888     88888888 "Y8888b.       #
#  Y88b 888 Y88b.   Y88b  d88P 888  888  888 d8b Y8b.          X88       #
#   "Y88888  "Y8888P "Y8888P"  888  888  888 Y8P  "Y8888   88888P\'       #
#                                                                        #
#                                                                        #
#                                                                        #
##########################################################################
############################ COSEC Alterdroid ############################
##########################################################################
'''
print alterdroid_logo


# -----------------------
# Alterdroid
# -----------------------

def get_apk(filename) :
    
    app = apk.APK(filename)
    return app

def get_dalvik_analysis(filename) :

    app = apk.APK(filename)		

    # parses classes.dex file of an Android application (APK).
    d = dvm.DalvikVMFormat( app.get_dex() )

    # analyze a dex
    dvmx = analysis.VMAnalysis( d )
    #udvmx = analysis.uVMAnalysis( d )

    # control flow graph
    gvmx = ganalysis.GVMAnalysis( dvmx, None )

    # setup references
    d.set_vmanalysis( dvmx )
    d.set_gvmanalysis( gvmx )

    # create xref/dref
    d.create_xref()
    d.create_dref()

    return app, d, dvmx, gvmx


def get_apk_info(apk, dvm, dvmx):
    
    if apk.is_valid_APK() :
        apk.show()
        #display_dvm_info( dvm, dvmx )
    else :
        print "INVALID APK"

def display_dvm_info(dvm, dvmx):
    
    print "Native code:", analysis.is_native_code(dvmx)
    print "Dynamic code:", analysis.is_dyn_code(dvmx)
    print "Reflection code:", analysis.is_reflection_code(dvmx)
    print "Ascii Obfuscation:", analysis.is_ascii_obfuscation(dvm)
    print "Crypto code:", analysis.is_crypto_code(dvm)
    
    #print "Native Methods: ", analysis.show_NativeMethods()
    #print "Dyn Code: ", analysis.show_DynCode()
    
    for i in dvmx.get_methods():
        i.create_tags()
        if not i.tags.empty():
            print i.method.get_class_name(), i.method.get_name(), i.tags

#def get_apk_components(apk, dvm, dvmx):
def get_apk_components(apk):
    
    file_type_key = {"Text Executable":["text", "executable"], "ELF Executable":["ELF", "executable"], "fount":["font"], "APK":["Android", "application", "package", "file"], "DEX":["Dalvik", "dex",  "file"]}
    
    types={}
    extensions={}
    components_of_interest=[]
    
    # COMPONENTS: activity, services, etc. 
    main_activity = apk.get_main_activity()
    activities = apk.get_activities()
    services = apk.get_services()
    receivers = apk.get_receivers
    providers = apk.get_providers()
    libraries = apk.get_libraries()
    
    # COMPONENTS: assets and resources components
    files=apk.get_files_types()
    for f in files:
        ##################
        # File extension #
        ##################
        fileName, fileExtension = os.path.splitext(f)
        try:
            extensions[fileExtension] = extensions[fileExtension] + 1
        except KeyError:
            extensions[fileExtension] = 1
        
        ##########################
        # Magic number extension #
        ##########################
        try:
            #print files[f]
            type = None
            for key in file_type_key:
                match = 0
                for token in file_type_key[key]:
                    if token in files[f]:
                        match = match + 1
                if len(file_type_key[key]) is match:
                    type = key
            if type == None:
                type = files[f].split(" ")[0]
            types[type] = types[type] + 1
        except KeyError:
            types[type] = 1

        #############################
        # Getting files of interest #
        #############################
    
        crc = apk.get_files_crc32()[f]
    
        for sub_class_name in FileCoI.FileCoI.__subclasses__():
            sub_class = sub_class_name(apk, fileName, fileExtension, type, files[f], f, crc)
            if sub_class.check():
                components_of_interest.append(sub_class)
            
    # COMPONENTS: variables
    # ... TODO ...


    print "\t - MAGIC N. FILES: ", types                
    print "\t - EXTENSIONS FILES: ", extensions
    print "\t - MAIN ACTIVITY: ", main_activity
    print "\t - ACTIVITIES", activities
    print "\t - SERVICES: ", services
    print "\t - RECEIVERS: ", receivers
    print "\t - PROVIDERS: ", providers
    print "\t - LIBRARIES: ", libraries
    print "\t - CMP OF INTEREST [", len(components_of_interest), "]: ", components_of_interest

    return types, extensions, main_activity, activities, services, receivers, providers, libraries, components_of_interest

def get_files_types(apk):
    files=apk.get_files_types()
    for f in files:
        print files[f]

def get_apk_fios2(cois):
    for coi in cois:
        for fio in FIOs.__all__:
            fio_module = getattr(sys.modules[FIOs.__name__], fio)
            
            for fio_klass_attr in inspect.getmembers(fio_module, inspect.isclass):
                fio_klass = getattr(fio_module, fio_klass_attr[0])
                print fio_klass
    
        print "-----------------"

def get_apk_fios(apk, cois, fast = False, delete_repackaged_apks = True):
    path_to_unpackaged_fio_dir = None
    repackaged_apks_with_injections = {}
    fio_fast_subtypes = []
    # Unpackage sample APK
    fio_unpackaged = FIOs.FIO.FIO(apk)
    path_to_output_dir, path_to_unpackaged_dir = fio_unpackaged.unpackage()
    # Test each Component of Interest
    for coi in cois:
        print "#######################################################################################"
        print "########## ", coi, " ##########"
        # for each Fault Injection Module
        for fio_module in FIO.FIO.__subclasses__():
            print " --- Injecting faults in: ", fio_module.__name__, " --- "
            # apply each fault injection available
            fios = fio_module(apk, coi.get_attributes()).get_fio_handler(coi, None)
            for fio in fios:
                print "\t - ", fio.get_sub_type()
                # apply the fault to the current path_to_unpackaged_dir
                try:
                    if fast:
                        path_to_unpackaged_fio_dir = fio.inject_fault(path_to_unpackaged_dir, path_to_unpackaged_dir)
                    else:
                        path_to_unpackaged_fio_dir = fio.inject_fault(path_to_unpackaged_dir)
                except Exception, e:
                    # -- delete path_to_output_dir
                    print_error =  "[WARNING FIO] Not possible to inject a fault at " + path_to_output_dir + " due to:  " + str(e)
                    print print_error
                    import traceback
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    traceback.print_tb(exc_traceback)
                    error = open('error.txt', 'a+')
                    error.write(print_error + "\n")
                    error.close()
                    if not fast:
                        try:
                            rmtree(path_to_unpackaged_fio_dir, onerror=error_deleting)
                        except:
                            pass
                    continue
                # if fast, repackage in a different apk
                if not fast:
                    path_to_repackaged_apk = fio.repackage(path_to_output_dir, path_to_unpackaged_fio_dir)
                    repackaged_apks_with_injections[path_to_repackaged_apk] = fio.get_sub_type()
                else:
                    fio_fast_subtypes.append(fio.get_sub_type())
                # remove unpackaged_fio_dir
                if not fast and delete_repackaged_apks:
                    rmtree(path_to_unpackaged_fio_dir, onerror=error_deleting)
            print "-------------------------------------------------"
    # if fast, apply all fios to the same un/repackaging
    if fast and path_to_unpackaged_fio_dir:
        path_to_repackaged_apk = fio_unpackaged.repackage(path_to_output_dir, path_to_unpackaged_fio_dir)
        repackaged_apks_with_injections[path_to_repackaged_apk] = fio_fast_subtypes
    if delete_repackaged_apks:
        rmtree(path_to_unpackaged_dir, onerror=error_deleting)
    print "#######################################################################################"
    return repackaged_apks_with_injections

def replace(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    new_file = open(abs_path,'w')
    old_file = open(file_path)
    for line in old_file:
        new_file.write(line.replace(pattern, subst))
    #close temp file and replace it for the original one
    new_file.close()
    close(fh)
    old_file.close()
    remove(file_path)
    move(abs_path, file_path)

def get_emulator_name(port='5554'):
    emulator = 'emulator-' + port
    return emulator

def init_port(ports):
    global PORT
    PORT = ports.get()
    print "initializing ", multiprocessing.current_process().name, os.getpid(), PORT

def get_port():
    return str(PORT)

def get_port_old():
    poolWorkerName = multiprocessing.current_process().name
    if "MainProcess" in poolWorkerName:
        return str(STARTING_POOL_PORT)
    else:
        strID=poolWorkerName[poolWorkerName.index('-')+1:]
        poolWorkerID = int(strID)
        #<port>+1 must be free and will be reserved for ADB.
        return str(STARTING_POOL_PORT + poolWorkerID*2)

def init_sandbox(port='5554'):
    stop_and_delete_sandbox(port)
    create_and_start_sandbox(port)
    # set the interrupt handler to stop the emulator after a signal...
    signal.signal(signal.SIGALRM, interruptHandler)
    signal.signal(signal.SIGINT, interruptHandler)

def stop_and_delete_sandbox(port):
    emulator = get_emulator_name(port)
    # Delete previous emulators (paranoid mode)
    call(['adb', '-s', emulator, 'emu', 'kill'])
    call(['android', '-s', 'delete', 'avd', '-n', alterdroid_prefix + '-' + emulator])
    call(['rm', '-r', 'imagesDroidboxTmp-' + emulator])
    print "> Wiped sandbox... ", alterdroid_prefix + '-' + emulator
    signal.alarm(0)

def create_and_start_sandbox(port, target10Not16=True):
    # Instatiating our sandbox
    emulator = get_emulator_name(port)
    imagesDroidboxTmp = 'imagesDroidboxTmp-' + emulator
    print "> Initiating our sandbox... ", emulator
    
    # Create new emulators and images
    # TODO: Print an error if user does not have 'android-10'... check: android list
    abi = 'armeabi' #'armeabi' #'x86'
    
    if target10Not16:
        p = Popen(['android', '-s', 'create', 'avd', '-n', alterdroid_prefix + '-' + emulator, '-f', '-c', '100M', '-t',  'android-10', '-b', abi], stdin=PIPE)
        print p.communicate(input='no\n')[0]
        imagesDroidbox = 'imagesDroidbox23'
        call(['cp', '-r', imagesDroidbox, imagesDroidboxTmp])
        #--Run custom kernel --
        Popen (['emulator', '-avd', alterdroid_prefix + '-' + emulator, '-system', imagesDroidboxTmp + '/system.img', '-kernel', imagesDroidboxTmp + '/zImage', '-wipe-data', '-prop', 'dalvik.vm.execution-mode=int:portable', '-port', port, '-no-boot-anim', '-no-window', '-noaudio'])
        #--Run default kernel --
        #Popen (['emulator', '-avd', alterdroid_prefix + '-' + emulator, '-system', imagesDroidboxTmp + '/system.img', '-wipe-data', '-prop', 'dalvik.vm.execution-mode=int:portable', '-port', port])
    else:
        p = Popen(['android', '-s', 'create', 'avd', '-n', alterdroid_prefix + '-' + emulator, '-f', '-c', '100M', '-t',  'android-16', '-b', abi], stdin=PIPE)
        print p.communicate(input='no\n')[0]
        imagesDroidbox = 'imagesDroidbox41'
        call(['cp', '-r', imagesDroidbox, imagesDroidboxTmp])
        print "> updating RAM of the emulator..."
        alterdoid_ini = "~/.android/avd/" + alterdroid_prefix + "-" + emulator + ".avd/config.ini"
        replace(os.path.expanduser(alterdoid_ini), "hw.heapSize=48", "hw.heapSize=128MB")
        replace(os.path.expanduser(alterdoid_ini), "hw.ramSize=512", "hw.ramSize=700MB")
        Popen (['emulator', '-avd', alterdroid_prefix + '-' + emulator, '-system', imagesDroidboxTmp + '/system.img', '-ramdisk', imagesDroidboxTmp + '/ramdisk.img', '-data', imagesDroidboxTmp + '/userdata-qemu.img', '-kernel', imagesDroidboxTmp + '/kernel-qemu', '-wipe-data', '-prop', 'dalvik.vm.execution-mode=int:portable', '-port', port, '-no-boot-anim', '-no-window', '-noaudio'])
    
    print "> Created new execution evironment. Wating for emulator..."
    
    # Wait for emulator and clean logcat
    call(['adb', '-s', emulator, 'wait-for-device'])
    
    bootcomplete = None
    count = 0
    while bootcomplete is not 1:
        p = Popen(['adb', '-s', emulator, 'wait-for-device', 'shell', 'getprop', 'sys.boot_completed'], stdout=PIPE)
        out=p.communicate()
        try:
            bootcomplete = int(str(out[0]).strip())
        except:
            pass
        sleep(2)
        count+=1
        if count % 10 is 0:
            print "Waiting boot completed... ", bootcomplete

    call(['adb', '-s', emulator, 'logcat', '-c'])


def interruptHandler(signum, frame):
    print "> Timeout: stopping analysis"
    stop_and_delete_sandbox('5554') #TODO: Pass argument port
    

def timeout( p ):
    print "terminate..."
    if p.poll() == None:
        try:
            p.terminate()
            p.kill()
            stop_and_delete_sandbox()
        except:
            pass


'''
    Executes a dynimic analysis using Droidbox and ouptus the signature.
'''
def get_activity_signature(app, duration, apk, port = None):
    
    # init sandbox if none one is given.
    # if one is give, should be initialized already.
    if port is None:
        port = get_port()
    init_sandbox(port)
    
    emulator = get_emulator_name(port)
    
    print "> Running analysis"
    call(['adb', '-s', emulator, 'logcat', '-c'])
    
    # start smart interaction
    interaction = InteractionThread(apk, port, False, duration) #TODO: fix AndroidViewClient with Droidbox images
    interaction.start()
    
    droidbox = DroidBox(app, 0)
    p = Popen(['adb', '-s', emulator, 'logcat', 'dalvikvm:W', 'OSNetworkSystem:W'], stdout=PIPE)
    
    # Stop the emulator after a duration...
    if duration and duration > 0:
        sleep(duration)
        stop_and_delete_sandbox(port)
        #signal.alarm(duration)
        # This should be reduntant than signal.alarm
        #t = threading.Timer( duration, timeout, [p] )
        #t.start()
        #t.join()
    
    #... meanwhile read the output of the sandbox (until the pipe is consumed)
    logcat, logcatErr = p.communicate()

    # When done: stop and process the output
    try:
        p.terminate()
        p.kill()
    except:
        pass
    try:
        interaction.stop()
        interaction.join(1)
    except:
        pass

    logcatIO = StringIO.StringIO(logcat)
    signature = droidbox.run(logcatIO)
    print "\t - SIG[" + app + "]:", signature

    return signature

'''
    # Python 3.3:
    try:
    logcat=p.communicate(timeout=duration)[0]
    except TimeoutExpired:
    print "1)", logcat
    p.kill()
    logcat = proc.communicate()[0]
    print "2)", logcat
'''



'''
    Runs several get_activity_signature executions.
'''
def get_activity_signatures(apps, duration, original_apk):
    signatures = {}
    for app in apps:
        signature = []
        if os.path.isfile(app):
            signature = get_activity_signature(app, duration, original_apk)
        signatures[app] = signature
    return signatures



def get_diff_signature(signature1, signature2):
    
    diff=[]
    map = {}
    alphabet = "abcdefghijklmnopqrstuvwABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    count = 0
    chain1 = ""
    chain2 = ""
    
    for e in signature1:
        try:
            map[e]
        except:
            if count >= len (alphabet):
                raise Exception ("Signature out of bounds.")
            map[e] = alphabet[count]
            count+=1
        chain1+=map[e]

    for e in signature2:
        try:
            map[e]
        except:
            if count >= len (alphabet):
                raise Exception ("Signature out of bounds.")
            map[e] = alphabet[count]
            count+=1
        chain2+=map[e]

    edit_operations = editops(chain1, chain2)
    for edit_operation in edit_operations:
        op_chain1 = [item[0] for item in map.viewitems() if item[1] == chain1[edit_operation[1]]]
        op_chain2 = [item[0] for item in map.viewitems() if item[1] == chain2[edit_operation[2]]] # not interested in this one
        property = [edit_operation[0], op_chain1[0]] 
        diff.append(property)

    return diff

def test_diff_signature():
    
    s1=["file-read", "file-read", "file-read", "file-read", "dexload", "file-read", "net-open", "file-read", "leak", "file-read", "file-read", "file-read", "net-open", "net-write", "file-read", "file-write", "net-open", "net-write", "net-write", "net-write", "net-write", "dexload", "net-open", "net-write", "net-write", "file-read", "file-read", "file-read", "file-read", "dexload", "file-read", "file-read", "net-open", "net-write", "file-read", "net-open", "net-write", "net-write", "net-write", "dexload", "net-write"]
    s2=["file-write", "file-write", "file-write", "file-write", "file-write", "file-write", "net-open", "net-write", "file-read", "file-write", "net-open", "net-write", "net-write", "net-write", "file-write", "net-write", "net-write"]
    return get_diff_signature(s1, s2)


def match_rules(diff, rules):
    positive_rules = []
    for rule in rules:
        if match_rule(diff, rule):
            positive_rules.append(rule["name"])
    return positive_rules


'''
    Matches a differential analysis with a rule
    For simplicity the rules does only have a set of AND rules and a set of OR rules.
    AND rules: all rules must match with at least one of the properties of the differential analysis.
    OR rules: at least one rule must match with the one of the properties of the differential analysis.
    TODO: This can be extended to provide a complete logical boolean formulation
'''
def match_rule(diff, rule):
    
    # get the AND properties of the rule
    try:
        and_properties = rule["and"]
    except:
        and_properties = []


    for property in and_properties:
        if property not in diff:
            return False
    
    # get the OR properties of the rule
    try:
        or_properties = rule["or"]
    except:
        or_properties = []

    for property in or_properties:
        if property in diff:
            return True

    return False


def get_summary_coi(cois):
    out = "[COIs] " + str(len(cois)) + "\n"
    for coi in cois:
        out = out + " + [COI] " + coi.get_type() + "(" + coi.get_sub_type() + "): " +  coi.toString() + "\n"
    return out

def get_summary_rep_injections(repackaged_apks_with_injections):
    fast = False
    try:
        assert isinstance(repackaged_apks_with_injections[app], basestring)
    except:
        fast = True

    if not repackaged_apks_with_injections:
        out = "[FIOs] 0\n"
    elif not fast:
        # one injection at a time
        out = "[FIOs] " + str(len(repackaged_apks_with_injections)) + "\n"
        for app in repackaged_apks_with_injections:
            out = out + " + [FIO] " + repackaged_apks_with_injections[app] + "\n"
    else:
        # all injections at the same time
        sample = repackaged_apks_with_injections.itervalues().next()
        out = "[FIOs] " + str(len(sample)) + "\n"
        for fio in sample:
            out = out + " + [FIO] " + fio + "\n"
    return out

def get_summary_execution(file_name, inittime, endstatic, endtime, components_of_interest, repackaged_apks_with_injections, signature_original):
    out = "=============================" + "\n"
    out += "[SAMPLE] " + file_name + "\n"
    #out += str(inittime) + "\n"
    out += endstatic + "\n"
    out += endtime + "\n"
    out += get_summary_coi(components_of_interest) + "\n"
    out += get_summary_rep_injections(repackaged_apks_with_injections) + "\n"
    out += "[ORIGINAL SIGNATURE] " + str(signature_original) + "\n"
    out += "\n"
    return out


def alterdroidCaseStudy(file_name, file_name_fio, duration):

    print "==== FILES ===="
    print "Original APP", file_name
    print "Fault APP", file_name_fio
    print 

    print "#### FILE ####"
    print "Original APP", file_name
    _apk, _dvm, _dvmx, _gvmx = get_dalvik_analysis(file_name)
    print "==== Stats ===="
    types, extensions, main_activity, activities, services, receivers, providers, libraries, components_of_interest = get_apk_components(_apk)
    print "# CoIs", len(components_of_interest)
    print "# Types", types
    print "# Extensions", extensions

    signature = get_activity_signature(file_name, duration, _apk)
    print "==== Signature ===="
    print signature
    print 

    print "#### FILE ####"
    print "Fault APP", file_name_fio

    _apk, _dvm, _dvmx, _gvmx = get_dalvik_analysis(file_name_fio)
    signature_fio = get_activity_signature(file_name_fio, duration, _apk)
    print "==== Signature FIO ===="
    print signature_fio

    print "==== Stats FIO ===="
    types, extensions, main_activity, activities, services, receivers, providers, libraries, components_of_interest = get_apk_components(_apk)
    print "# CoIs", len(components_of_interest)
    print "# Types", types
    print "# Extensions", extensions
    print 

    print "#### BOTH FILES ####"
    diff = get_diff_signature(signature, signature_fio)
    print "==== Difference ===="
    print diff 

    print "==== Rules ===="
    print match_rules(diff, rules)



def alterdroidTest(file_name, duration):
    # .............................
    _apk, _dvm, _dvmx, _gvmx = get_dalvik_analysis(file_name)
    #print_info(_apk, _dvm, _dvmx)
    # .............................
    #_apk, _dvm, _dvmx, _gvmx = get_dalvik_analysis("samples/AnserverBot/0b43ce26e3d71472de357ab4b1e3aa0b94d9f445.apk")
    #fio = FIOs.FIO.FIO(_apk)
    #path_to_output_dir, path_to_unpackaged_dir = fio.unpackage()
    # .............................
    #fio = FIOs.FIO.FIO(_apk)
    #path_to_output_dir, path_to_unpackaged_dir = fio.unpackage()
    #path_to_repackaged_apk = fio.repackage("/home/guillermo/Desktop/", path_to_unpackaged_dir)
    # .............................
    #fio = FIOs.FIO.FIO(_apk)
    #path_to_output_dir, path_to_unpackaged_dir = fio.unpackage()
    #path_to_repackaged_apk = fio.repackage(path_to_output_dir, path_to_unpackaged_dir)
    # .............................
    #fio = FIOs.FIO.FIO(_apk)
    #path_to_output_dir, path_to_unpackaged_dir = fio.unpackage()
    #path_to_repackaged_apk = fio.repackage(None, path_to_unpackaged_dir)
    # .............................
    #coi = CoI.CoI(_apk)
    #print coi.shannonEntropyImageFile('samples_repackaged/samples_GingerMaster_2f77b540c49b1b9be16ecf03c14254fd951d4e03/sample/res/drawable-hdpi/back.png')
    # .............................
    #print file_name
    #types, extensions, main_activity, activities, services, receivers, providers, libraries, components_of_interest = get_apk_components(_apk)
    #print get_summary_coi(components_of_interest)
    #coi = components_of_interest[0]
    #print(coi)
    #print(coi.get_attributes())
    #fio = FIOs.FileFIO.GenericMutationFile(_apk, coi.get_attributes(), coi, None)
    #path_to_output_dir, path_to_unpackaged_dir = fio.unpackage()
    #fio.inject_fault(path_to_unpackaged_dir, path_to_output_dir)
    #path_to_repackaged_apk = fio.repackage(None, path_to_unpackaged_dir)
    # .............................
    #init_sandbox()
    #interaction = InteractionThread(_apk, '5554', False)
    #interaction.start()
    # .............................
    #get_activity_signature(options.input, duration, _apk)
    # .............................
    #test_diff_signature()
    # .............................
    #test_view_clientDump(options.input)
    # .............................
    #positive_rules = match_rules(test_diff_signature(), rules)
    #print positive_rules
    # .............................
    #print(get_summary_coi(components_of_interest))
    # .............................
    #repackaged_apks_with_injections = get_apk_fios(_apk, components_of_interest)
    #print(get_summary_rep_injections(repackaged_apks_with_injections))
    # .............................
    #file_test = "droidbox/DroidBoxTests.apk"
    #_apk, _dvm, _dvmx, _gvmx = get_dalvik_analysis(file_test)
    #signature = get_activity_signature(file_test, 2*60, _apk)
    #print signature
    # .............................
    #file_test = "samples/GingerMaster/2e9b8a7a149fcb6bd2367ac36e98a904d4c5e482.apk"
    #_apk, _dvm, _dvmx, _gvmx = get_dalvik_analysis(file_test)
    #signature = get_activity_signature(file_test, 0, _apk)
    #print signature
    # .............................
    #file_test = "samples/GingerMaster/2e9b8a7a149fcb6bd2367ac36e98a904d4c5e482.apk"
    #_apk, _dvm, _dvmx, _gvmx = get_dalvik_analysis(file_test)
    #signature = get_activity_signature(file_test, 120, _apk)
    #_apk, _dvm, _dvmx, _gvmx = get_dalvik_analysis(file_test)
    #signature2 = get_activity_signature(file_test, 120, _apk)
    #diff = get_diff_signature(signature, signature2)
    #print match_rules(diff, rules)
    #print signature
    # .............................
    print "End tests"

def alterdroidStatsTest(file_name, duration):
    
    _apk, _dvm, _dvmx, _gvmx = get_dalvik_analysis(file_name)
    types, extensions, main_activity, activities, services, receivers, providers, libraries, components_of_interest = get_apk_components(_apk)
    return  len(components_of_interest)

def error_deleting(func, path, excinfo):
    error = open('error.txt', 'a+')
    error.write("[ERROR RM] " + str(excinfo) + " at " + str(path)  + "\n")
    error.close()

'''
    Controller: Checks the components of interest
    (see components_of_interest()) and injects
    the FIO (see get_apk_fios())
    '''
def alterdroid(file_name, duration, fast = False, delete_repackaged_apks = True): 
    
    print " -------------------------------------- "
    print file_name, ":"
    
    try:
        
        #TODO: check duration
        
        # -- report results
        report = open('report.txt', 'a')
        history = open('history.txt', 'a')
        history.write(file_name + '\n')
        history.close()
        
        '''
            Static Analisys ---------------------------------------
        '''
        inittime = time.time()
        _apk = get_apk(file_name)
        # -- get components:
        types, extensions, main_activity, activities, services, receivers, providers, libraries, components_of_interest = get_apk_components(_apk)
        if len(components_of_interest) is 0:
            print "No Components of Interest found for this app!"
            out = get_summary_execution(file_name, 0, "[DURATION] Static: 0", "[DURATION] Dynamic: 0\n", [], {}, [])
            report.write(out)
            report.write("\n\n")
            report.close()
            return
        # -- get Fault Injection Operators (FIOs):
        repackaged_apks_with_injections = get_apk_fios(_apk, components_of_interest, fast, delete_repackaged_apks)
        statictime = time.time()
        endstatic = "[DURATION] Static: " + str(statictime-inittime)
        print endstatic
        
        '''
            Dynamic Analisys ---------------------------------------
        '''
        signature_original = []
        signature_modified = {}
        
        if repackaged_apks_with_injections and os.path.isfile(file_name):
            signature_original = get_activity_signature(file_name, duration, _apk)
            signature_modified = get_activity_signatures(repackaged_apks_with_injections, duration, _apk)
        enddynamic = time.time()
        endtime = "[DURATION] Dynamic: " + str(enddynamic-statictime)
        total = "[DURATION] All: " + str(enddynamic-inittime)
        print total
        
        
        # -- report results
        out = get_summary_execution(file_name, inittime, endstatic, endtime, components_of_interest, repackaged_apks_with_injections, signature_original)
        if len(repackaged_apks_with_injections) is 0:
            print "No signatures found, perhaps there was no Fault Injection Operators defined for this app!"
            report.write(out)
            report.write("\n\n")
            report.close()
            return
        path_to_repackaged_apk = os.path.dirname(repackaged_apks_with_injections.keys()[0])
        report_name = path_to_repackaged_apk + ".txt"
        reportSample = open(report_name, 'w')
        
        # -- delete repackaged_apks_with_injections
        if (delete_repackaged_apks):
            rmtree(path_to_repackaged_apk, onerror=error_deleting)
        
        '''
            Differential Signatures -------------------------------
        '''
        for signature in signature_modified:
            diff = get_diff_signature(signature_original, signature)
            positive_rules = match_rules(diff, rules)
            
            # -- report results
            print positive_rules, ": ", diff
            out += "[REPACKAGED APP] " + os.path.basename(signature) + "\n"
            out += " + [REPACKAGED SIGNATURE] " + str(signature_modified[signature]) + "\n"
            out += " + [DIFFERENTIAL SIGNATURE] " + str(diff) + "\n"
            out += " + [SIG. MATCHING RULES] " + str(positive_rules) + "\n"
            out += "\n"
                
        report.write(out)
        reportSample.write(out)
        report.write("\n\n")
        report.close()
        reportSample.close()
        return len(components_of_interest)
    
    except Exception, e :
        error = open('error.txt', 'a+')
        error.write('[ERROR ALTERDROID]' + str(e) + '\n')
        error.close()
        print "ERROR", e
        import traceback
        traceback.print_exc()
        try:
            # -- delete repackaged_apks_with_injections
            if (delete_repackaged_apks):
                rmtree(path_to_repackaged_apk, onerror=error_deleting)
            report.close()
            reportSample.close()
        except:
            pass

'''
   Print some info about the app. Useful for debuging.
'''
def print_info(_apk, _dvm, _dvmx):
    get_apk_info(_apk, _dvm, _dvmx)
    get_files_types(_apk)


def main(options, arguments) :

    fast = False
    clean = False
    
    if options.port != None:
        STARTING_POOL_PORT = int(options.port)

    if options.time == None:
        duration = 0
    else:
        duration = int(options.time)

    if options.fast != None:
        fast = True

    if options.clean != None:
        clean = True

    if options.caseStudy != None:
        ports = Queue()
        ports.put(5554)
        init_port(ports)
        alterdroidCaseStudy(options.caseStudy[0], options.caseStudy[1], duration)

    elif options.input != None :
        ret_type = androconf.is_android( options.input )
        if ret_type == "APK" :
            ports = Queue()
            ports.put(5554)
            init_port(ports)
            alterdroid(options.input, duration, fast, clean)
            #!#alterdroidTest(options.input, duration)
            #!#alterdroidStatsTest(options.input, duration)

    elif options.directory != None :
        real_filenames = []
        for root, dirs, files in os.walk( options.directory, followlinks=True ):
            # -- Get real path to valid apks
            if files != [] :
                for f in files :
                    real_filename = root
                    if real_filename[-1] != "/" :
                        real_filename += "/"
                    real_filename += f
                    ret_type = androconf.is_android( real_filename )
                    if ret_type == "APK"  :
                        real_filenames.append(real_filename)

        try:
            history = open('history.txt')
            history_filenames = [line.strip('\n') for line in history]
            try:
                history_filenames.remove('')
            except ValueError:
                pass
            history.close()
        except IOError:
            history_filenames = []

        try:
            num_cpus = multiprocessing.cpu_count()
        except NotImplementedError:
            num_cpus = 2
        
        # -- Create pool of process:
        '''
            NOTE: The emulator console port number must be an even integer between 5554 and 5584, inclusive. <port>+1 must also be free and will be reserved for ADB. Thus, we can only parallize 15 emulators
        '''
        print 'Creating pool with %d processes\n' % PROCESSES
        pool = multiprocessing.Pool(PROCESSES, initializer=init_port, initargs=(PORTS,)) #, maxtasksperchild=100

        # -- Create tasks from all apks
        TASKS = [(f, duration, fast, clean) for f in set(real_filenames).symmetric_difference(history_filenames)]

        # -- Process all tasks
        results = [pool.apply_async(alterdroid, t) for t in TASKS]
        
        # -- Print results... equivalent than pool.apply as blocks the task
        #for r in results:
            #print '\t', r.get(), '\n'
        pool.close()
        pool.join()

    elif options.version != None :
        print "Alterdroid version %s" % version
        print "Androguard version %s" % androconf.ANDROGUARD_VERSION
        print "Apktool version %s" % FIO.PATH_TO_APKTOOL
        print "AndroidViewClient version %s" % vc.__version__
        print "Droidbox version 'modified version'"


if __name__ == "__main__" :
    if len(sys.argv[:]) <= 1:
        print "Try with option -h"
        exit()
    parser = OptionParser()
    for option in options:
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    #sys.argv[:] = arguments
    report = open('report.txt', 'w')
    report.write(alterdroid_logo)
    report.close()
    error = open('error.txt', 'w')
    error.close()
    main(options, arguments)
