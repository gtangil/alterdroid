#!/usr/bin/env python
###############################################################################################
# (c) 2014, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid. All rights reserverd.
# Main Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
###############################################################################################

import sys, math, os

from sets import Set
from PIL import Image
from tempfile import mkstemp
from subprocess import Popen, PIPE, STDOUT, call


# TODO: This is a bit ugly, but it works for the moment.
PATH_TO_STEGOSW = "libs/stegdetect/"
STEGOSW = "stegdetect"

STEGOSW_LIB = os.path.join(os.getcwd(), PATH_TO_STEGOSW)
PATH =  os.environ.get("PATH", '')
PATH = PATH + ":" + STEGOSW_LIB


# ------------------

class CoI(object):
    def __init__(self, apk):
        self.type = None
        self.apk = apk

    def get_apk(self):
        return self.apk

    def get_type(self):
        return self.type


    '''
    Returns the Shannon Entropy of the input
    '''
    def shannonEntropy(self, input_string):
        
        # calculate the frequency of each symbol in the string
        inputStrList = list(input_string)
        alphabet = list(Set(inputStrList))
        freqList = []
        for symbol in alphabet:
            ctr = 0
            for sym in inputStrList:
                if sym == symbol:
                    ctr += 1
            freqList.append(float(ctr) / len(inputStrList))
    
        # Shannon entropy
        ent = 0.0
        for freq in freqList:
            ent = ent + freq * math.log(freq, 2)
        ent = -ent
        
        return ent


    '''
    Stego-image according to Stegdetect (http://www.outguess.org/)
    '''
    def steganalysis(self, input_string):

        ret = [False, "None"]

        # -- create tmp file and stores the stream
        fh, abs_path = mkstemp(prefix='alterdroid_img')
        img_out_tmp = open(abs_path, 'w')
        img_out_tmp.write(input_string)
        img_out_tmp.close()

        # -- apply steganalysis with Stegdetect
        p = Popen([STEGOSW, abs_path], env={"PATH": PATH}, stdout=PIPE)
        out = p.communicate()
        
        # -- process result
        try:
            result = str(out[0]).split(':')[1].strip()
            if result.startswith('skipped'):
                ret[1] = "skipped"
            elif result.startswith('negative'):
                ret[1] = "negative"
            else:
                ret[0] = True
                ret[1] = result
        except Exception, e:
            print_error =  "[ERROR STEGANALYSIS] Not possible to analyze the file due to (" + str(out) + "): " + str(e)
            print print_error
            import traceback
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_tb(exc_traceback)
            error = open('error.txt', 'a+')
            error.write(print_error + "\n")
            error.close()
    
        # -- clean tmp file and return
        os.remove(abs_path)
        return ret

'''
    def shannonEntropyImageFile(self, img_file_path):
        
        #Snannon Entropy for Red, Green, Blue:
        im = Image.open(img_file_path)
        rgbHistogram = im.histogram()
        ent_rgb = []
        for rgb in range(3):
            totalPixels = sum(rgbHistogram[rgb * 256 : (rgb + 1) * 256])
            ent = 0.0
            for col in range(rgb * 256, (rgb + 1) * 256):
                freq = float(rgbHistogram[col]) / totalPixels
                if freq > 0:
                    ent = ent + freq * math.log(freq, 2)
            ent = -ent
            ent_rgb.append(ent)
        return ent_rgb
'''