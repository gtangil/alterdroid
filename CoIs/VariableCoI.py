#!/usr/bin/env python
###############################################################################################
# (c) 2014, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid. All rights reserverd.
# Main Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
###############################################################################################

import sys

from CoI import CoI

# ------------------

class VariableCoI(CoI):
    def __init__(self, apk, klass, variable_type, variable_name, variable_value):
        self.__init__(self, apk)
        self.type = "VariableCoI"
        self.klass = klass
        self.variable_type = variable_type
        self.variable_name = variable_name
        self.variable_value = variable_value

    def toString(self):
        return self.variable_type + "/" + self.variable_name + "/" + self.variable_value

class TemplateVariableCheck(VariableCoI):
    
    def sub_type(self):
        return self.__class__.__name__
    
    def check(self):
        print  "do the check"
        return False


