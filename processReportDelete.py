
#!/usr/bin/env python
################################################################################
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
################################################################################

import sys, os, StringIO

from tempfile import mkstemp
from shutil import move
from os import remove, close

from pylab import *

monitoring_duration = 120.0

def get_summary_boxPlot(file_name, sample, filter=None):
    
    num_samples = 0.0
    average_duration = 0.0 # per fio
    cois = 0.0
    fios = 0.0
    num_matching_rules = 0.0
    false_positives = 0
    true_positives = 0
    
    cois_type_per_app = {}
    fios_type_per_app = {}
    duration_per_app = []
    
    rules_all = []
    rules_count = {}
    
    max_cois_per_app = 0.0
    max_cois_per_app_type = {}
    max_cois_inter_app = 0.0
    
    report = open(file_name, 'r')
    report_out = open(file_name+".filtered.txt", 'w')
    lines = []
    while True:
        lines_buffer = []
        line = report.readline()
        if not line: break
        if line[0] is '#' or line[0] is '=':
            lines.append(line)
        else:
            lines_buffer.append(line)
        if '[SAMPLE]' in line and sample in line:
            #Init counters of this sample
            cois_per_type = {}
            fios_per_type = {}
            
            #Dynamic and Static duration
            line = report.readline()
            lines_buffer.append(line)
            duration_static = float(str(line.split('[DURATION] Static:')[1]).strip())
            line = report.readline()
            lines_buffer.append(line)
            duration_dynamic = float(str(line.split('[DURATION] Dynamic:')[1]).strip())
            
            #COIs
            line = report.readline()
            lines_buffer.append(line)
            while line and not 'COIs' in line:
                if not line: break
                line = report.readline()
                lines_buffer.append(line)
            coi = float(str(line.split('[COIs]')[1]).strip())
            cois += coi
            # -- COI
            line = report.readline()
            lines_buffer.append(line)
            while line and '[COI]' in line:
                coi_type = line[line.index('(')+1: line.index(')')]
                if line:
                    try:
                        num=cois_per_type[coi_type]
                    except KeyError:
                        num=0
                    cois_per_type[coi_type]=num+1
                    if cois_per_type[coi_type] > max_cois_inter_app:
                        max_cois_inter_app = cois_per_type[coi_type]
                line = report.readline()
                lines_buffer.append(line)

            if max_cois_per_app < coi:
                max_cois_per_app = coi
                max_cois_per_app_type = {}
                max_cois_per_app_type = cois_per_type

            #FIOs
            line = report.readline()
            lines_buffer.append(line)
            while line and not 'FIOs' in line:
                if not line: break
                line = report.readline()
                lines_buffer.append(line)
            num_fios = float(str(line.split('[FIOs]')[1]).strip())
            fios += num_fios
            # -- FIO
            line = report.readline()
            lines_buffer.append(line)
            while line and '[FIO]' in line:
                fio_type = str(line.split('[FIO]')[1]).strip()
                if line:
                    try:
                        num=fios_per_type[fio_type]
                    except KeyError:
                        num=0
                    fios_per_type[fio_type]=num+1
                line = report.readline()
                lines_buffer.append(line)

            #Duration
            app_duration = (duration_static + duration_dynamic)/(num_fios+1)
            average_duration += app_duration
            #average_duration += (duration_static + duration_dynamic - monitoring_duration)/(num_fios+1)
            
            #Matching Rules
            detections = 0
            line = report.readline()
            lines_buffer.append(line)
            while '=============================' not in line:
                if not line: break
                if '[REPACKAGED SIGNATURE] []' in line:
                    lines_buffer = []
                if '[SIG. MATCHING RULES]' in line:
                    rules = str(line.split(' + [SIG. MATCHING RULES] ')[1]).replace('[','').replace(']','').replace(' ','').replace('\n','').replace('\'','').split(',')
                    for item in rules:
                        isInRules = True
                        if item is '':
                            rules.remove('')
                            isInRules = False
                        if filter and item in filter:
                            rules.remove(item)
                            isInRules = False
                        if isInRules:
                            rules_all.append(item)
                            try:
                                count = rules_count[item]
                            except:
                                count = 0
                            rules_count[item] = count + 1
                    #print sample, rules, len(rules)
                    num_matching_rules += len(rules)
                    if len(rules) > 0:
                        detections += 1
                line = report.readline()
                if len(lines_buffer) > 0:
                    lines_buffer.append(line)
            if detections > 0:
                false_positives+=1
            else:
                true_positives+=1
            num_samples += 1

            # Accumulate global states
            for f in cois_per_type:
                try:
                    l = cois_type_per_app[f]
                except KeyError:
                    l = []
                l.append(cois_per_type[f])
                cois_type_per_app[f]=l
            #for i in range(len(cois_type_per_app), int(num_samples)):
            #    cois_type_per_app.append(0)

            for f in fios_per_type:
                try:
                    l = fios_type_per_app[f]
                except KeyError:
                    l = []
                l.append(fios_per_type[f])
                fios_type_per_app[f]=l

            duration_per_app.append(app_duration)

            for line in lines_buffer:
                lines.append(line)

    for line in lines:
        report_out.write(line)

    # All vectors should be the same length, so reminder elements (until num_samples) are appended with 0
    for c in cois_type_per_app:
        for i in range(len(cois_type_per_app[c]), int(num_samples)):
            cois_type_per_app[c].append(0)

    # All vectors should be the same length, so reminder elements (until num_samples) are appended with 0
    for f in fios_type_per_app:
        for i in range(len(fios_type_per_app[f]), int(num_samples)):
            fios_type_per_app[f].append(0)

    # Calculate the number of apps per COI
    apps_per_coi_and_type = {}
    for c in cois_type_per_app:
        apps_per_coi = [0] * (max_cois_inter_app + 1)
        for i in range(0, len(apps_per_coi)):
            # count the number of apps that has i CoIs
            apps_per_coi[i] = cois_type_per_app[c].count(i)
        apps_per_coi_and_type[c] = apps_per_coi

    report.close()
    report_out.close()
    return num_samples, average_duration, cois, fios, rules_count, num_matching_rules, false_positives, true_positives, cois_type_per_app, fios_type_per_app, duration_per_app, max_cois_per_app, max_cois_per_app_type, apps_per_coi_and_type


def process(file_name, report):
    report_tag = "[" + report + "]"
    try:
        get_summary_boxPlot(file_name, report)

    except ZeroDivisionError:
        print report_tag + " --- No samples found ---"
    pass


reportVS = "VirusShare"
#file_name = "/Users/gtangil/Dropbox/ToDo/Alterdroid/reportVirusShareStatic/report_virusshare.txt" #sys.argv[1]
file_name = "/Users/guille/Dropbox/ToDo/Alterdroid/reportImagesVirusShare3K.txt"
print "# Processing ", file_name
process(file_name, reportVS)
