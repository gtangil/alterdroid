
#!/usr/bin/env python
################################################################################
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
################################################################################

import sys, os, StringIO

from tempfile import mkstemp
from shutil import move
from os import remove, close

from pylab import *

def extractHistory(file_name):
    
    report = open(file_name, 'r')
    report_out = open("history.txt", 'a')
    
    while True:
        line = report.readline()
        if not line: break

        if '[SAMPLE]' in line:
            print line.strip('\n')
            line_hist = line.split('[SAMPLE]')[1].strip()
            report_out.write(line_hist+'\n')

    report.close()
    report_out.close()


file_name = "/Users/guille/Dropbox/ToDo/Alterdroid/reportVirusShare1.5K.txt"
print "# Processing ", file_name
extractHistory(file_name)
