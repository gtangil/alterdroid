#!/usr/bin/env bash
cPATH=`pwd`
export ANDROID_VIEW_CLIENT_HOME="$cPATH/libs/AndroidViewClient"
adb kill-server
pkill adb
adb start-server
touch history.txt
python alterdroid.py "$@"
